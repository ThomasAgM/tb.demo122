﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExoBonus
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exo 1.1
            //int a = 5;
            //int b = 35;
            //int c = 0;

            //Console.WriteLine(a);
            //Console.WriteLine(b);

            //c = a;
            //a = b;
            //b = c;


            //Console.WriteLine(a);
            //Console.WriteLine(b);

            //Console.ReadKey();
            #endregion


            #region Exo 1.2
            //Console.WriteLine("Veuillez entrer un nombre, il resortira comme par magie au carré !");

            //string nombre = Console.ReadLine(); 

            //int nb = int.Parse(nombre);

            //Console.WriteLine("La magie a dit que votre résultat est :" + (nb * nb));

            //Console.ReadKey();
            #endregion


            #region Exo 2.1

            //while (true)
            //{
            //    Console.WriteLine("Veuillez entrer votre premier nombre");
            //    string premierNombre = Console.ReadLine();



            //    Console.WriteLine("Veuillez entrer votre opérateur");

            //    string operateur = Console.ReadLine();



            //    Console.WriteLine("Veuillez entrer votre deuxième nombre");

            //    string deuxiemeNombre = Console.ReadLine();



            //    int first = int.Parse(premierNombre);
            //    int second = int.Parse(deuxiemeNombre);

            //    int resultat = 0; 


            //    if (operateur == "+")
            //    {
            //        resultat = (first + second);
            //        Console.WriteLine("La réponse est : " + resultat);
            //    }



            //    else if (operateur == "-")
            //    {
            //        resultat = (first - second);
            //        Console.WriteLine("La réponse est : " + resultat);
            //    }

            //    else if (operateur == "*")
            //    {
            //        resultat = (first * second);
            //        Console.WriteLine("La réponse est : " + resultat);
            //    }

            //    else if (operateur == "/")
            //    {
            //        if (second == 0)
            //        {
            //            Console.WriteLine("Vers l'infini et au delà ! ");
            //        }
            //        else
            //        {
            //            resultat = (first / second);
            //            Console.WriteLine("La réponse est : " + resultat);
            //        }
            //    }
            //}
            #endregion


            #region Exo 2.2
            //double date = (DateTime.Now.Hour);

            //if (date >= 9 && date < 12 || date >= 13 && date < 17)
            //{
            //    Console.WriteLine("Au boulot ! And have fun !");
            //}

            //else if (date >= 12 && date <= 13)
            //{
            //    Console.WriteLine("Bon app'!");
            //}

            //else if (date >= 17 && date < 9)
            //{
            //    Console.WriteLine("ZzZz");
            //}

            //Console.ReadLine();

            #endregion


            #region Exo 3.1

            //string[] tab = new string[8];
            //tab[0] = "Merci";
            //tab[1] = "Khun";
            //tab[2] = "Pour";
            //tab[3] = "Ta";
            //tab[4] = "Patience";
            //tab[5] = "Et";
            //tab[6] = "Les";
            //tab[7] = "Exo";

            ////for
            //for (int i = 0; i < 8; i++)
            //{
            //    Console.WriteLine(tab[i]);
            //}


            //////foreach
            ////foreach (string item in tab)
            ////{
            ////    Console.WriteLine(item);
            ////}

            //Console.ReadLine(); 
            #endregion


            #region Exo 3.2

            //Console.WriteLine("Veuillez entrer un nombre");
            //string nombre = Console.ReadLine();

            //int nb = int.Parse(nombre);

            //Console.Clear();

            //for (int i = 1; i <= 20; i++)
            //{
            //    Console.WriteLine(nb * i);
            //}
            //Console.ReadLine();
            #endregion


            #region Exo 3.3

            //for (int z = 1; z <= 10; z++)
            //{
            //    for (int i = 1; i <= 10; i++)
            //    {
            //        Console.WriteLine(i * z);
            //    }
            //}
            //Console.ReadLine();
            #endregion


            #region Exo 3.4

            //Console.WriteLine("Entrez autant que nombre que vous voulez, entrez un caractère pour arrêter le programme et connaitre la somme de tout vos nombre");

            //string nombre = Console.ReadLine();

            //int result = 0;

            //while (int.TryParse(nombre, out int nb))
            //{
            //    result = result + nb;
            //    nombre = Console.ReadLine();
            //}
            //Console.WriteLine("La somme total de vos nombres est de  : " + result);

            //Console.ReadLine();
            #endregion


            #region Exo 3.5
            //int[] tab = new int[5];

            //for (int v = 0; v < 5; v++)
            //{
            //    Console.WriteLine("Veuillez entrer un nombre");
            //    string nombre = Console.ReadLine();
            //    int nb = int.Parse(nombre);
            //    tab[v] = nb;
            //}


            //for (int i = 0; i < 5; i++)
            //{

            //    bool go = true;
            //    while (i >= 0 && i < 4 && go)
            //    {
            //        if (tab[i] > tab[i + 1])

            //        {
            //            int temp = tab[i];
            //            tab[i] = tab[i + 1];
            //            tab[i + 1] = temp;
            //            i -= 1;
            //        }
            //        else
            //        {
            //            go = false;
            //        }

            //    }

            //}
            //for (int x = 0; x < 5; x++)
            //{
            //    Console.WriteLine(tab[x]);
            //}

            //Console.ReadLine();

            #endregion


            string fileName = "historique.txt";

            List<string> historique = new List<string>();


            if (!File.Exists(fileName))
            {
                FileStream fs = File.Create(fileName);
                fs.Close();
            }

            historique = File.ReadAllLines(fileName).ToList();


            while (true)
            {
                Console.WriteLine("Veuillez entrer votre premier nombre");
                string premierNombre = Console.ReadLine();

                Console.WriteLine("Veuillez entrer votre opérateur");

                string operateur = Console.ReadLine();

                Console.WriteLine("Veuillez entrer votre deuxième nombre");

                string deuxiemeNombre = Console.ReadLine();

                int first = int.Parse(premierNombre);
                int second = int.Parse(deuxiemeNombre);
                


                int resultat = 0;


                if (operateur == "+")
                {
                    resultat = (first + second);
                    string resultat1 = resultat.ToString();

                    historique.Add($"{first} + {second} = {resultat}");
                    Console.WriteLine("La réponse est : " + resultat);
                }


                else if (operateur == "-")
                {
                    resultat = (first - second);

                    historique.Add($"{first} - {second} = {resultat}");
                    Console.WriteLine("La réponse est : " + resultat);
                }

                else if (operateur == "*")
                {
                    resultat = (first * second);
                    Console.WriteLine("La réponse est : " + resultat);
                }

                else if (operateur == "/")
                {
                    if (second == 0)
                    {
                        Console.WriteLine("Vers l'infini et au delà ! ");
                    }
                    else
                    {
                        resultat = (first / second);
                        Console.WriteLine("La réponse est : " + resultat);
                    }
                }

            }
            //Réécris tout
            File.WriteAllLines(fileName, historique);
            //Ajoute
            //File.AppendAllLines(fileName, historique);
        }
    }
}
