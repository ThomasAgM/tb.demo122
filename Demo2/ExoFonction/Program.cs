﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoFonction
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exo 1.1
            //Console.WriteLine("Quel est votre âge?");
            //int age = int.Parse(Console.ReadLine());

            //Console.WriteLine("Quel est votre nom?");
            //string nom = Console.ReadLine();

            //presentation(nom, age);

            //Console.ReadKey(); 
            #endregion


            #region Exo 1.2
            List<int> nombre = new List<int>();

            Console.WriteLine("Veuillez entrer vos nombre.\nEnter pour afficher");
            string entree = Console.ReadLine();

            bool chaineVide = false;

            while (chaineVide == false)
            {
                if (entree != string.Empty)
                {
                    int nbr = int.Parse(entree);
                    nombre.Add(nbr);
                    entree = Console.ReadLine();
                }
                else
                {

                    double moyenne = calculMoyenne(nombre);
                    Console.WriteLine(moyenne);
                    chaineVide = true;
                }
            }
            Console.ReadKey();

            #endregion


            #region Exo 1.3
            //List<int> nombre = new List<int>();

            //Console.WriteLine("Entrez vos nombre");

            //int nbr = int.Parse(Console.ReadLine());

            //nombre.Add(nbr);

            //listex2(nombre);

            //Console.ReadKey();

            #endregion


            #region Exo 1.4
            //List<int> nombre = new List<int>();

            //Console.WriteLine("Quels sont vos nombre?");
            //string entree = Console.ReadLine();
            //int nbr;
            //while (int.TryParse(entree, out nbr))
            //{
            //    nombre.Add(nbr);
            //    entree = Console.ReadLine();
            //}

            //Console.Clear();

            //foreach (int item in nombre)
            //{
            //    Console.WriteLine(item);
            //}

            //listcarre(nombre);

            //Console.ReadLine(); 
            #endregion
        }

        public static void listcarre(List<int> nombre)
        {
            for (int i = 0; i < nombre.Count; i++)
            {
                nombre[i] = nombre[i] * nombre[i];
            }

            foreach (int item in nombre)
            {
                Console.WriteLine(item);
            }
        }

        public static void listex2(List<int> nombre)
        {
            foreach (int item in nombre)
            {
                Console.WriteLine(item*2);
            }
        }

        public static double calculMoyenne(List<int> nombre)
        {
            double total = 0;
            foreach (int item in nombre)
            {

                total += item;
            }
            total = total / nombre.Count();

            return total;


        public static void presentation(string nom, int age)
        {
            Console.WriteLine($"Bonjour, je m'appelle {nom} et j'ai {age} ans");
        }
    }
}
