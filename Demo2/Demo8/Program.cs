﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo8
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> menu = new Dictionary<string, int>();


            while (true)
            {
                Console.WriteLine("1:Ajouter produit et stock");
                Console.WriteLine("2:Modifier stock");
                Console.WriteLine("3:Supprimer produit");
                string choix = Console.ReadLine();
                int choixu;

                while (!int.TryParse(choix, out choixu))
                {
                    Console.WriteLine("Entrez un choix valide");
                    choix = Console.ReadLine();
                }

                #region Ajouter
                if (choixu == 1)
                {

                    Console.WriteLine("Veuillez entrer le nom de votre bière.\nEnter pour quitter");
                    string nom = Console.ReadLine();
                    nom = nom.ToUpper();

                    while (menu.ContainsKey(nom))
                    {
                        Console.WriteLine("Le produit existe déjà");
                        nom = Console.ReadLine();
                    }


                    Console.WriteLine("Et la quantité ajoutée au stock ?");
                    string qt = Console.ReadLine();

                    int quantite;

                    while (!int.TryParse(qt, out quantite))
                    {
                        Console.WriteLine("Quantité non valide");
                        qt = Console.ReadLine();
                    }

                    menu.Add(nom, quantite);

                }
                #endregion

                #region Modifier stock
                else if (choixu == 2)
                {
                    Console.WriteLine("De quel produit voulez vous modifier le stock ?");
                    string cStock = Console.ReadLine();
                    cStock = cStock.ToUpper();

                    if (menu.ContainsKey(cStock))
                    {
                        Console.WriteLine("1: enlever du stock\n2: ajouter au stock");
                        string enlajout = Console.ReadLine();
                        int enlever;
                        while (!int.TryParse(enlajout, out enlever))
                        {
                            Console.WriteLine("Veuillez un choix valide");
                            enlajout = Console.ReadLine();
                        }

                        if (enlever == 1)
                        {
                            Console.WriteLine("Quelle quantitée voulez vous enlever?");
                            string qtaenl = Console.ReadLine();
                            int quantenl;
                            while (!int.TryParse(qtaenl, out quantenl))
                            {
                                Console.WriteLine("Veuillez entrer une quantité valide");
                                qtaenl = Console.ReadLine();
                            }

                            menu[cStock] -= quantenl;

                        }

                        else if (enlever == 2)
                        {
                            Console.WriteLine("Quelle quantitée à ajouter?");
                            string qtaajout = Console.ReadLine();
                            int quantajo;
                            while (!int.TryParse(qtaajout, out quantajo))
                            {
                                Console.WriteLine("Veuillez entrer une quantité valide");
                                qtaajout = Console.ReadLine();
                            }

                            menu[cStock] += quantajo;
                        }

                    }
                }
                #endregion

                #region Supprimer produit
                else if (choixu == 3)
                {
                    Console.WriteLine("Veuillez entrer le produit à supprimer");
                    string supp = Console.ReadLine();
                    supp = supp.ToUpper();
                    while (!menu.ContainsKey(supp))
                    {
                        Console.WriteLine("Veuillez entrer un produit existant");
                        supp = Console.ReadLine();
                        supp = supp.ToUpper();
                    }
                    menu.Remove(supp);
                } 
                #endregion


                Console.Clear();
                foreach (KeyValuePair<string, int> kvp in menu)
                {
                    Console.WriteLine($"Au menu une :{kvp.Key} au nombre de : {kvp.Value}");
                }


            }







        }

    }
}

