﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoFonction2
{
    class Program
    {
        static void Main(string[] args)
        {
            int nb = 4;
            SePresenter("Thomas",26);
            //NombrePaire(nb);
            Console.ReadKey();
        }

        public static bool NombrePaire(int p)
        {
            bool paire;

            if (p % 2 == 0)
            {
                paire = true;
                return paire;
            }
            else
            {
                paire = false;
                return paire;
            }

        }
        public static void SePresenter(string nom, int age)
        {
            Console.WriteLine($"Bonjour je m'appelle {nom} et j'ai {age} ans");
        }
    }
}
