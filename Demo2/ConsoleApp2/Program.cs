﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int nb = 4;

            NombrePaire(nb);
            Console.ReadKey();
        }

        public static bool NombrePaire(int p)
        {
            bool paire;

            if (p % 2 == 0)
            {
                paire = true;
                return paire;
            }
            else
            {
                paire = false;
                return paire;
            }

        }
    }
}
