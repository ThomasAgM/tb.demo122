﻿using Production.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Production
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public ObservableCollection<Carte> Cartes { get; set; }

        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }
        public int Timer { get; set; }

        public int Tour { get; set; } = 0;
        public int index1 { get; set; } = -1;

        private Random rng = new Random();

        public void Shuffle(int nb)
        {
            int n = Cartes.Count;
            while (nb > 0)
            {
                nb--;
                int k = rng.Next(n);
                int k1 = rng.Next(n);
                Carte value = Cartes[k];
                Cartes[k] = Cartes[k1];
                Cartes[k1] = value;
            }
        }
        public MainWindow()
        {
            InitializeComponent();

            Start = DateTime.Now;

            Image Joker = new Image();
            Joker.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\Joker.jpg"));

            Image Chat = new Image();
            Chat.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\Chat.jpg"));

            Image Koala = new Image();
            Koala.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\Koala.jpg"));

            Image Renard = new Image();
            Renard.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\Renard.png"));

            Image Chien = new Image();
            Chien.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\Chien.png"));

            Cartes = new ObservableCollection<Carte>();
            Cartes.Add(new Carte { Nom = Chat });
            Cartes.Add(new Carte { Nom = Chat });
            Cartes.Add(new Carte { Nom = Koala });
            Cartes.Add(new Carte { Nom = Koala });
            Cartes.Add(new Carte { Nom = Renard });
            Cartes.Add(new Carte { Nom = Renard });
            Cartes.Add(new Carte { Nom = Chien });
            Cartes.Add(new Carte { Nom = Chien });
            Cartes.Add(new Carte { Nom = Joker });

            Shuffle(1000);

        }

        private void RetournerCarte(object sender, RoutedEventArgs e)
        {

            Image img = new Image();
            img.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\dos.jpg"));

            Button b = (sender as Button);
            int index = int.Parse(b.CommandParameter.ToString());
            b.Content = Cartes[index].Nom;
            if (index1 == -1)
            {
                index1 = index;
            }
            else
            {
                if (index1 != index)
                {
                    if (Cartes[index1].Nom == Cartes[index].Nom)
                    {
                        MessageBox.Show("Gagné");
                        Tour++;
                    }
                    else
                    {
                        b.Content = img;
                        Button b1 = GD.Children[index1] as Button;
                        Image img2 = new Image();
                        img2.Source = new BitmapImage(new Uri(@"C:\Users\AgM122\Pictures\ImageProg\dos.jpg"));
                        b1.Content = img2;
                    }
                }
                else
                {
                    b.Content = img;
                }
                index1 = -1;
            }


            if (Tour >= 4)
            {
                Stop = DateTime.Now;
                Timer = Start.Second - Stop.Second;
                MessageBox.Show($"Vous avez gagné en :{Timer} secondes");
            }
        }

    }
}
