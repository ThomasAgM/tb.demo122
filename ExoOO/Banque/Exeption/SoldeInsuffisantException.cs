﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Exeption
{
    class SoldeInsuffisantException: Exception
    {
        public SoldeInsuffisantException(): base("Votre solde est insuffisant")
        {

        }
    }
}
