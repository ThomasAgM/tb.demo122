﻿
using Banque.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Banque
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Bonjour, veuillez entrer le nom de votre banque");
            string nomB = Console.ReadLine();
            Banks b = new Banks()
            {
                Nom = nomB
            };

            Console.WriteLine("Veuillez entrer votre Nom");
            string nom = Console.ReadLine();
            Console.WriteLine("Veuillez entrer votre Prénom");
            string prenom = Console.ReadLine();
            Console.WriteLine("Veuillez entrer votre date de naissance sous le formet 'yyyy,mm,dd");
            string date1 = Console.ReadLine();
            DateTime date;
            DateTime.TryParse(date1, out date);
 

            Personne user = new Personne();
            user.Nom = nom;
            user.Prenom = prenom;
            user.DateNaiss = date;
            
            Console.WriteLine("Veuillez entrer votre ligne de credit");
            double credit = double.Parse(Console.ReadLine());
            Console.WriteLine("Veuillez entrer votre numéro de compte");
            string num = Console.ReadLine();

            Courant compte = null;
            try
            {
                    compte = new Courant
                    (
                        credit,
                        num,
                        0,
                        user
                    );
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            b.Ajouter(compte);


            Console.Clear();
            Console.WriteLine("1: depot\n2: retrait\n3: Ajouter compte\n4: Supprimer compte\n5: Rechercher compte\n6: sortir");
            int choix = int.Parse(Console.ReadLine());
            while(choix != 6)
            {
                if (choix == 1)
                {
                    Console.Clear();
                    Console.WriteLine($"Le solde actuel est de : {compte.Solde} euro");
                    Console.WriteLine("Combien voulez vous déposer?");
                    double montant = double.Parse(Console.ReadLine());
                    compte.Depot(montant);
                    Console.Clear();
                    Console.WriteLine($"Le nouveau solde est de : {compte.Solde} euro");
                }

                else if (choix == 2)
                {
                    Console.Clear();
                    Console.WriteLine($"Le solde actuel est de : {compte.Solde} euro");
                    Console.WriteLine("Combien voulez vous retirer?");
                    double montantR = double.Parse(Console.ReadLine());
                    if(montantR <= compte.Solde)
                    {
                        Console.Clear();
                        compte.Retrait(montantR);
                        Console.WriteLine($"Le nouveau solde est de : {compte.Solde} euro");
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Solde insuffisant");
                    }
                }
                else if(choix == 3)
                {
                    Console.Clear();
                    Console.WriteLine("Veuillez entrer votre ligne de credit");
                    double credit2 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Veuillez entrer votre numéro de compte");
                    string num2 = Console.ReadLine();
                    Courant compte2 = new Courant
                    (
                        credit,
                        num,
                        0,
                        user
                    );

                    b.Ajouter(compte2);
                    Console.Clear();
                }
                else if (choix == 4)
                {
                    Console.Clear();
                    Console.WriteLine("Veuillez entrer le numéro de compte à supprimer");
                    string numeroA = Console.ReadLine();
                    try
                    {
                        b.Retirer(numeroA);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    Console.Clear();
                }
                else if (choix == 5)
                {
                    Console.Clear();
                    Console.WriteLine("Veuillez entrer le numéro de compte à rechercher");
                    string numAC = Console.ReadLine();
                    b.Rechercher(numAC); 
                }
                Console.WriteLine("1: Depot\n2: Retrait\n3: Ajouter compte\n4: Supprimer compte\n5: Rechercher compte\n6: Sortir");
                choix = int.Parse(Console.ReadLine());

            }

        }
    }
}
