﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    class Banks
    {
        public string Nom { get; set; }

        public Dictionary<string, Comptes> Comptes { get; private set; }
            = new Dictionary<string, Comptes>();

        public void Ajouter(Comptes c)
        {
            if (Comptes.ContainsKey(c.Numero))
            {
                Console.WriteLine("Le numéro de compte existe déjà");
            }
            else
            {
                Comptes.Add(c.Numero, c);
            }
        }

        public void Retirer(string numero)
        {
            if (!Comptes.ContainsKey(numero))
            {
                Console.WriteLine("Ce compte n'existe pas");
            }
            else
            {
                Comptes.Remove(numero);
            }
        }

        public Comptes Rechercher(string numero)
        {
            if (!Comptes.ContainsKey(numero))
            {
                Console.WriteLine("Ce compte n'existe pas");
                return null;
            }
            else
            {
                Comptes result = Comptes[numero];
                return result;
            }

        }
    }
}
