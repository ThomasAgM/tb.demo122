﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
     class Epargne : Comptes
    {
        private DateTime _dateRetrait;

        public Epargne(string numero, double solde, Personne titulaire) : base(numero, solde, titulaire)
        {
        }

        public DateTime DateRetrait

        {
            get { return _dateRetrait; }
            private set { _dateRetrait = value; }
        }

        public override void Retrait(double Montant)
        {
            base.Retrait(Montant);
            DateRetrait = DateTime.Now;
        }

        protected override double CalculInteret()
        {
            double interet = (Solde * 100) / 4.5;
            return interet;
        }
    }
}
