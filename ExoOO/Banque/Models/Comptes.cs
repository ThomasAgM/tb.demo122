﻿using Banque.Exeption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    abstract class Comptes : ICustomer, IBanker
    {
        private string _numero;
        private double _solde;
        private Personne _titulaire;

        public Comptes(string numero, double solde, Personne titulaire)
        {
            Numero = numero;
            Solde = solde;
            Titulaire = titulaire;
        }

        public string Numero
        {
            get
            {
                return _numero;
            }
            private set
            {
                _numero = value;
            }
        }

        public double Solde
        {
            get
            {
                return _solde;
            }
            private set
            {
                _solde = value;
            }
        }

        public Personne Titulaire
        {
            get
            {
                return _titulaire;
            }
            private set
            {
                _titulaire = value;
            }
        }


        public virtual void Retrait(double Montant)
        {
            if (Montant <= 0)
            {
                //Console.WriteLine("Le montant ne peut pas être négatif ou égale à 0");
                throw new ArgumentException();
            }

            else if(Montant > Solde)
            {
                throw new SoldeInsuffisantException();
            }
            else
            {
            Solde = Solde - Montant;
            }
        }

        public void Depot(double Montant)
        {
            if (Montant <= 0)
            {
                //Console.WriteLine("Le montant ne peut pas être négatif ou égale à 0");
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                Solde = Solde + Montant;
            }
        }

        protected abstract double CalculInteret();

        public void AppliquerInteret()
        {
            double interet = CalculInteret();
            Solde += interet;
        }



    }
}
