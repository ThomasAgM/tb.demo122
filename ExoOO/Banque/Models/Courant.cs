﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
     class Courant : Comptes
    {
        private double _ligneDeCredit = -500;

        public Courant(double ligneDeCredit, string numero, double solde, Personne titulaire) : base(numero, solde, titulaire)
        {
            LigneDeCredit = ligneDeCredit;
        }

        private double LigneDeCredit
        {
            get
            {
                return _ligneDeCredit;
            }
             set
            {
                if (value >= 0)
                {
                    _ligneDeCredit = value;
                }
                else
                {
                    //Console.WriteLine("La valeur entrée ne peut pas être négative");
                    throw new InvalidOperationException();
                }
            }
        }
        public override void Retrait(double Montant)
        {
            
            if (Solde - Montant <= _ligneDeCredit)
            {
                Console.WriteLine("Solde insuffisant");
            }
            else
            {
                base.Retrait(Montant);
            }
        }

        protected override double CalculInteret()
        {
            if (Solde < 0)
            {
                double interet = (Solde * 100) / 9.75;
                return interet;
            }

            else 
            {
                double interet = (Solde * 100) / 3;
                return interet;
            }
        }
    }
}
