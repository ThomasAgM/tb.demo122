﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vrai_ou_faux
{
    class Program
    {
        static void Main(string[] args)
        {


            #region Question
            string[,] tableauQ = new string[10, 3];
            tableauQ[0, 0] = "Sereno à un chien.";
            tableauQ[1, 0] = "ctrl + K + V définit une région.";
            tableauQ[2, 0] = "Nath est Hollandaise";
            tableauQ[3, 0] = "maVariable.GetType récupère le type de la variable.";
            tableauQ[4, 0] = "Lum porte 50kg max";
            tableauQ[5, 0] = "Le CTRL + Z sauve la vie des dev";
            tableauQ[6, 0] = "Tous les raccourcis de Mathieu fonctionnent.";
            tableauQ[7, 0] = "Les tableaux sont dynamiques en C#";
            tableauQ[8, 0] = "Khun est un as du ménage.";
            tableauQ[9, 0] = "Dans la déclaration du tableau, ligne en premier ?";
            #endregion


            #region Correction
            tableauQ[0, 1] = "Sereno a bien un chien";
            tableauQ[1, 1] = "C'est CTRL + K + S ";
            tableauQ[2, 1] = "Nath porte des sandales chaussettes.";
            tableauQ[3, 1] = "maVariable.GetType récupère bien le type de la variable.";
            tableauQ[4, 1] = "Il en porte minimum 75 !";
            tableauQ[5, 1] = "Et tous les jours en plus !";
            tableauQ[6, 1] = "Aucun,en fait...";
            tableauQ[7, 1] = "Absolument pas ! La taille d'un tableau est prédéfinie.";
            tableauQ[8, 1] = "Il a pris du retard dans ses tâches.";
            tableauQ[9, 1] = "Par convention des mathématiciens ce sont les colonnes en premier contrairement à la convention des physiciens ! Mais ce ne sont que des conventions, prends le plus logique pour toi.";
            #endregion


            #region Réponse
            tableauQ[0, 2] = "1";
            tableauQ[1, 2] = "2";
            tableauQ[2, 2] = "1";
            tableauQ[3, 2] = "1";
            tableauQ[4, 2] = "2";
            tableauQ[5, 2] = "1";
            tableauQ[6, 2] = "2";
            tableauQ[7, 2] = "2";
            tableauQ[8, 2] = "2";
            tableauQ[9, 2] = "2";
            #endregion



            string[] tableauR = new string[] { "1" };
            int score = 0;
            Console.WriteLine("Bonjour et Bienvenue au jeu du Vrai ou Faux \nRépondez par '1' pour vrai ou '2' pour faux");

            for (int i = 0; i < tableauQ.GetLength(0); i++)
            {
                Console.WriteLine(tableauQ[i,0]);
                string r = Console.ReadLine();
                while (r != "1" && r != "2"){

                Console.WriteLine("Veuillez entrer une proposition valide");
                 r = Console.ReadLine();
                }

                if (r == tableauQ[i,2])
                {
                    Console.WriteLine("C'est une bonne réponse !");
                    Console.WriteLine("");
                    score ++;

                }
                
                else
                {
                    Console.WriteLine("Mauvaise réponse ! \nLa réponse est :" + tableauQ[i, 1]);
                    Console.WriteLine("");


                }


            }

            Console.WriteLine($"votre score est de {score} / {tableauQ.GetLength(0)} ");
            Console.ReadKey();









          
            
            

        }
    }
}
