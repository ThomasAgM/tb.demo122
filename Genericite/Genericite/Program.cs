﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genericite
{
    class Program
    {
        static void Main(string[] args)
        {
            Forge<Fer> ForgeFer = new Forge<Fer>();
            Forge<Or> ForgeOr = new Forge<Or>();
            Forge<Argent> ForgeArgent = new Forge<Argent>();

            ForgeFer.Fondre(new Fer());
            ForgeArgent.Fondre(new Argent());
            ForgeOr.Fondre(new Or());

            Console.ReadKey();
        }
    }
}
