﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genericite
{
    class Forge<T>
        where T: new()
    {
        public void Fondre(T o)
        {
            Console.WriteLine("Je fais fondre du " + o.GetType());
        }

        public T ProduireDesBarres()
        {
            return new T();
        }
    }
}
