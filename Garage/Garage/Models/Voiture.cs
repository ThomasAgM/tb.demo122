﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Models
{
    class Voiture: Vehicule
    {
        public int NombrePlace { get; set; }

        public override double CalculerPrixHoraire()
        {
            return NombrePlace * 10;
        }
    }
}
