﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Models
{
    class Facture
    {
        public Vehicule vehicule { get; set; }

        public List<Reparation> Repa { get; set; } = new List<Reparation>();


        private double CalculerCoutTotal()
        {
            int heure = 0;
            foreach (Reparation item in Repa)
            {
                heure += item.Duree;
            }
            if (Repa.Count < 6)
            {
                return heure * vehicule.CalculerPrixHoraire();
            }
            else
            {
                double total = heure * vehicule.CalculerPrixHoraire();
                double reduc = total * 0.1;
                return total - reduc;
            }
        }

        public void AfficherFacture()
        {
            if (vehicule is Camion)
            {
                foreach (Reparation item in Repa)
                {
                    Console.WriteLine($"{item.Intitule} - {item.Duree}h");
                }
                int km = (vehicule as Camion).NombresKm;
                string kmT = km.ToString();
                Console.WriteLine("La facture s'élève à : " + CalculerCoutTotal() + " euros" + " et vous êtes à : " + kmT + " km");
            }
            else if (vehicule is Voiture)
            {
                foreach (Reparation item in Repa)
                {
                    Console.WriteLine($"{item.Intitule} - {item.Duree}h");
                }
                int nb = (vehicule as Voiture).NombrePlace;
                Console.WriteLine("La facture s'élève à : " + CalculerCoutTotal() + " euros" + " Pour une voiture de : " + nb + " places");
            }
        }

    }
}
