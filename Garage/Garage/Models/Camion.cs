﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Models
{
    class Camion: Vehicule
    {
        public int NombresKm { get; set; }

        public override double CalculerPrixHoraire()
        {
            if(NombresKm <= 200000)
            {
                double prix = 50;
                return prix;
            }
            else
            {
                double prix = 75;
                return prix;
            }
        }
    }
}
