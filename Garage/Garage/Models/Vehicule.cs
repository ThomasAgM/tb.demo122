﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Models
{
    abstract class Vehicule 
    {
        public Personne Proprio { get; set; }
        public string Modele { get; set; }
        public string Immatriculation { get; set; }

        public abstract double CalculerPrixHoraire();

    }
}
