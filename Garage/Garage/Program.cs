﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garage
{
    class Program
    {
        static void Main(string[] args)
        {
            Facture f = new Facture();

            Console.WriteLine("Bonjour, veuillez entre votre nom suivi de votre prénom");
            Personne p = new Personne();
            p.Nom = Console.ReadLine();
            p.Prenom = Console.ReadLine();
            Console.WriteLine("Veuillez entrer votre date de naissance sous le format 'yyyy,mm,dd'");
            DateTime date = DateTime.Parse(Console.ReadLine());
            while (date > DateTime.Now)
            {
                Console.WriteLine("Veuillez entrer une date correct.");
                date = DateTime.Parse(Console.ReadLine());
            }

            Console.WriteLine("1: camion\n2: voiture");
            int choix = int.Parse(Console.ReadLine());

            Vehicule v = null;

            if (choix == 1)
            {
                v = new Camion();
            }
            else
            {
                v = new Voiture();
            }
            if (v is Voiture)
            {
                Console.WriteLine("Quel est le nombre de place?");
                int place = int.Parse(Console.ReadLine());
                (v as Voiture).NombrePlace = place;
            }
            else if (v is Camion)
            {
                Console.WriteLine("Quel est le nombre de km ?");
                int nb = int.Parse(Console.ReadLine());
                (v as Camion).NombresKm = nb;
            }

            v.Proprio = p;
            Console.WriteLine("Quel est le modèle de votre véhicule ?");
            v.Modele = Console.ReadLine();
            Console.WriteLine("Quel est votre plaque d'immatriculation?");
            string imma = Console.ReadLine();
            char[] tab = imma.ToCharArray();
            while (tab.Length < 7 && tab.Length >7)
            {
                Console.WriteLine("Veuillez entrer une plaque à 7 caractère");
                imma = Console.ReadLine();
            }
            string immatriculation = tab.ToString();
            v.Immatriculation = immatriculation;

            f.vehicule = v;

            

            bool chaineVide = false;

            while (chaineVide == false)
            {
                Console.WriteLine("Veuillez entrer le nom de votre réparation. Enter pour finir.");
                string rep = Console.ReadLine();
                if (rep != string.Empty)
                {
                    Reparation repa = new Reparation();
                    repa.Intitule = rep;
                    Console.WriteLine("Veuillez entrer la durée en heure de la réparation");
                    int dureeRepa = int.Parse(Console.ReadLine());
                    repa.Duree = dureeRepa;
                    f.Repa.Add(repa);
                    Console.Clear();
                }
                else
                {
                    chaineVide = true;
                }
            }
            f.AfficherFacture();
            Console.ReadKey();
        }
    }
}
