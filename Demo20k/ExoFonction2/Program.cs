﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ExoFonction2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exo 1.1
            //int nb = 17;
            //Console.WriteLine(NombrePaire(nb));
            //Console.ReadKey(); 
            #endregion

            #region Exo 1.2
            //List<int> Liste = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //List<int> liste5 = new List<int>();
            //liste5 = PasDeNombreImpair(Liste);
            //foreach (int item in liste5)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.ReadKey(); 
            #endregion

            #region Exo 1.3
            string ana = Anagramme("Vers l'infini et au delà");
            Console.WriteLine(ana);
            Console.ReadKey(); 
            #endregion

            #region Exo 1.4
            //string crypte = Crypt("ceci fut complique");
            //Console.WriteLine(crypte);
            //string uncry = Uncrypt(crypte);
            //Console.WriteLine(uncry);
            //Console.ReadKey(); 
            #endregion

            #region Exo 2
            //string fichierCode = "code.txt";
            //if (!File.Exists(fichierCode))
            //{
            //    FileStream fs = File.Create(fichierCode);
            //    fs.Close();
            //}

            //EncodeFichier(fichierCode);


            //string code = "code2.txt";
            //if (!File.Exists(code))
            //{
            //    FileStream fs = File.Create(code);
            //    fs.Close();
            //}

            //DecoderFichier(code); 
            #endregion
        }

        public static string Anagramme(string v)
        {

                        Random nb = new Random();
            int ite = nb.Next(0,1000);

            char[] tab = v.ToCharArray();

            int itera = nb.Next(0, tab.Length);
            int j = 0;
            while (j < ite)
            {
                char tempo = ' ';

                for (int i = 0; i < tab.Length - 1; i++)
                {
                    tempo = tab[i];
                    tab[i] = tab[itera];
                    tab[itera] = tempo;
                }
                j++;
            }
            string retour = new string(tab);
            return retour;
        }

        public static void DecoderFichier(string code)
        {
            #region Dico
            Dictionary<char, char> dico = new Dictionary<char, char>();
            dico.Add('a', 'l');
            dico.Add('b', 'x');
            dico.Add('c', 'd');
            dico.Add('d', 'u');
            dico.Add('e', 'p');
            dico.Add('f', 't');
            dico.Add('g', 'r');
            dico.Add('h', 's');
            dico.Add('i', 'v');
            dico.Add('j', 'w');
            dico.Add('k', 'a');
            dico.Add('l', 'o');
            dico.Add('m', 'i');
            dico.Add('n', 'j');
            dico.Add('o', 'm');
            dico.Add('p', 'n');
            dico.Add('q', 'b');
            dico.Add('r', 'c');
            dico.Add('s', 'z');
            dico.Add('t', 'h');
            dico.Add('u', 'g');
            dico.Add('v', 'f');
            dico.Add('w', 'e');
            dico.Add('x', 'k');
            dico.Add('y', 'q');
            dico.Add('z', 'y');
            #endregion

            string fichierDecode = "Decode.txt";
            if (!File.Exists(fichierDecode))
            {
                FileStream fs = File.Create(fichierDecode);
                fs.Close();
            }
            List<string> liste = File.ReadAllLines(code).ToList();
            List<char> listeC = new List<char>();
            List<char> listeK = dico.Keys.ToList();
            List<string> listeE = new List<string>();
            foreach (string item in liste)
            {
                char[] tab = item.ToCharArray();
                for (int i = 0; i < tab.Length; i++)
                {
                    foreach (KeyValuePair<char, char> kvp in dico)
                    {
                        if (kvp.Value == tab[i])
                        {
                            listeC.Add((kvp.Key));
                        }
                    }
                }
            }
            string mots = "";
            for (int i = 0; i < listeC.Count; i++)
            {
                mots = mots + listeC[i].ToString();

            }
            listeE.Add(mots);

            File.WriteAllLines(fichierDecode, listeE);

        }

        public static void EncodeFichier(string fichierCode)
        {
            #region Dico
            Dictionary<char, char> dico = new Dictionary<char, char>();
            dico.Add('a', 'l');
            dico.Add('b', 'x');
            dico.Add('c', 'd');
            dico.Add('d', 'u');
            dico.Add('e', 'p');
            dico.Add('f', 't');
            dico.Add('g', 'r');
            dico.Add('h', 's');
            dico.Add('i', 'v');
            dico.Add('j', 'w');
            dico.Add('k', 'a');
            dico.Add('l', 'o');
            dico.Add('m', 'i');
            dico.Add('n', 'j');
            dico.Add('o', 'm');
            dico.Add('p', 'n');
            dico.Add('q', 'b');
            dico.Add('r', 'c');
            dico.Add('s', 'z');
            dico.Add('t', 'h');
            dico.Add('u', 'g');
            dico.Add('v', 'f');
            dico.Add('w', 'e');
            dico.Add('x', 'k');
            dico.Add('y', 'q');
            dico.Add('z', 'y'); 
            #endregion

            string fichierEncode = "Encode.txt";
            if (!File.Exists(fichierEncode))
            {
                FileStream fs = File.Create(fichierEncode);
                fs.Close();
            }

            List<string> liste = File.ReadAllLines(fichierCode).ToList();
            List<char> listeC = new List<char>();
            List<char> listeK = dico.Keys.ToList();
            List<string> listeE = new List<string>();
            foreach (string item in liste)
            {
                char[] tab = item.ToCharArray();
                for (int i = 0; i < tab.Length; i++)
                {
                    foreach (KeyValuePair<char, char> kvp in dico)
                    {
                        if (kvp.Key == tab[i])
                        {
                            listeC.Add((kvp.Value));
                        }                       
                    }
                }
            }
            string mots = "";
            for (int i = 0; i < listeC.Count; i++)
            {
                mots = mots + listeC[i].ToString();
            }
            listeE.Add(mots);
            File.WriteAllLines(fichierEncode, listeE);          
        }

        public static string Uncrypt(string crypte)
        {
            #region Dico
            Dictionary<char, char> dico = new Dictionary<char, char>();
            dico.Add('a', 'l');
            dico.Add('b', 'x');
            dico.Add('c', 'd');
            dico.Add('d', 'u');
            dico.Add('e', 'p');
            dico.Add('f', 't');
            dico.Add('g', 'r');
            dico.Add('h', 's');
            dico.Add('i', 'v');
            dico.Add('j', 'w');
            dico.Add('k', 'a');
            dico.Add('l', 'o');
            dico.Add('m', 'i');
            dico.Add('n', 'j');
            dico.Add('o', 'm');
            dico.Add('p', 'n');
            dico.Add('q', 'b');
            dico.Add('r', 'c');
            dico.Add('s', 'z');
            dico.Add('t', 'h');
            dico.Add('u', 'g');
            dico.Add('v', 'f');
            dico.Add('w', 'e');
            dico.Add('x', 'k');
            dico.Add('y', 'q');
            dico.Add('z', 'y');
            #endregion

            char[] tab = crypte.ToCharArray();
            string retour = "";

            for (int i = 0; i < tab.Length; i++)
            {
                foreach (KeyValuePair < char,char> kvp in dico)
                {
                    if(kvp.Value == tab[i])
                    {
                        retour = retour + kvp.Key;
                    }
                }
            }
            return retour;

        }

        public static string Crypt(string v)
        {
            #region dico
            Dictionary<char, char> dico = new Dictionary<char, char>();
            dico.Add('a', 'l');
            dico.Add('b', 'x');
            dico.Add('c', 'd');
            dico.Add('d', 'u');
            dico.Add('e', 'p');
            dico.Add('f', 't');
            dico.Add('g', 'r');
            dico.Add('h', 's');
            dico.Add('i', 'v');
            dico.Add('j', 'w');
            dico.Add('k', 'a');
            dico.Add('l', 'o');
            dico.Add('m', 'i');
            dico.Add('n', 'j');
            dico.Add('o', 'm');
            dico.Add('p', 'n');
            dico.Add('q', 'b');
            dico.Add('r', 'c');
            dico.Add('s', 'z');
            dico.Add('t', 'h');
            dico.Add('u', 'g');
            dico.Add('v', 'f');
            dico.Add('w', 'e');
            dico.Add('x', 'k');
            dico.Add('y', 'q');
            dico.Add('z', 'y');
            #endregion

            char[] tab = v.ToCharArray();
            List<char> liste = dico.Keys.ToList();
            string retour = "";
            for (int i = 0; i < tab.Length; i++)
            {
                foreach (char item in liste)
                {
                    if(item == tab[i])
                    {
                        retour = retour + dico[item];
                    }
                }
            }
            return retour;
        }

        public static List<int> PasDeNombreImpair(List<int> liste2)
        {
            List<int> paire = new List<int>();
            foreach (int item in liste2)
            {
                if(item%2 == 0)
                {
                    paire.Add(item);
                }
            }
            return paire;
        }

        public static bool NombrePaire(int nb)
        {
            if(nb%2 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
